/* Copyright (c) 2014, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _PANEL_HX8389BG_QHD_VIDEO_H_
#define _PANEL_HX8389BG_QHD_VIDEO_H_
/*---------------------------------------------------------------------------*/
/* HEADER files                                                              */
/*---------------------------------------------------------------------------*/
#include "panel.h"

/*---------------------------------------------------------------------------*/
/* Panel configuration                                                       */
/*---------------------------------------------------------------------------*/
static struct panel_config hx8389bg_qhd_video_panel_data = {
	"qcom,mdss_dsi_hx8389bg_qhd_video", "dsi:0:", "qcom,mdss-dsi-panel",
	10, 0, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel resolution                                                          */
/*---------------------------------------------------------------------------*/
static struct panel_resolution hx8389bg_qhd_video_panel_res = {
	540, 960, 90, 90, 30, 0, 10, 10, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel color information                                                   */
/*---------------------------------------------------------------------------*/
static struct color_info hx8389bg_qhd_video_color = {
	24, 0, 0xff, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel on/off command information                                          */
/*---------------------------------------------------------------------------*/

static char hx8389bg_qhd_video_on_cmd0[] = {
	0x04, 0x00, 0x39, 0xC0,
	0xB9, 0xFF, 0x83, 0x89,

};

static char hx8389bg_qhd_video_on_cmd1[] = {
	0x08, 0x00, 0x39, 0xC0,
	0xBA, 0x41, 0x83, 0x00,
	0x16, 0xA4, 0x00, 0x18,

};

static char hx8389bg_qhd_video_on_cmd2[] = {
	0x14, 0x00, 0x39, 0xC0,
	0xB1, 0x00, 0x00, 0x04,
	0xE8, 0x50, 0x10, 0x11,
	0xB1, 0xF1, 0x36, 0x3E,
	0x28, 0x28, 0x42, 0x01,
	0x5A, 0xF7, 0x00, 0xE6,

};



static char hx8389bg_qhd_video_on_cmd3[] = {
	0x08, 0x00, 0x39, 0xC0,
	0xB2, 0x00, 0x00, 0x78,
	0x08, 0x0B, 0x3F, 0x80,

};

static char hx8389bg_qhd_video_on_cmd4[] = {
	0x20, 0x00, 0x39, 0xC0,
	0xB4, 0x80, 0x08, 0x00,
	0x32, 0x10, 0x06, 0x30,
	0x13, 0xD2, 0x00, 0x00,
	0x00, 0x3F, 0x0A, 0x40,
	0x08, 0x37, 0x0A, 0x40,
	0x14, 0xFF, 0xFF, 0x0A,
	0x0A, 0x40, 0x0A, 0x40,
	0x14, 0x46, 0x50, 0x0A,

};

static char hx8389bg_qhd_video_on_cmd5[] = {
	0x04, 0x00, 0x39, 0xC0,
	0xB7, 0x00, 0x00, 0x50,

};

static char hx8389bg_qhd_video_on_cmd6[] = {
	0x31, 0x00, 0x39, 0xC0,
	0xD5, 0x00, 0x00, 0x00,
	0x00, 0x01, 0x00, 0x00,
	0x00, 0xA0, 0x10, 0x88,
	0x88, 0x88, 0x01, 0x08,
	0x48, 0x81, 0x85, 0x28,
	0x68, 0x83, 0x87, 0x05,
	0x88, 0x48, 0x88, 0x88,
	0x88, 0x88, 0x88, 0x88,
	0x88, 0x10, 0x78, 0x38,
	0x86, 0x82, 0x58, 0x10,
	0x84, 0x80, 0x84, 0x80,
	0x58, 0x88, 0x88, 0x88,
	0x88, 0xFF, 0xFF, 0xFF,
};

static char hx8389bg_qhd_video_on_cmd7[] = {
	0x23, 0x00, 0x39, 0xC0,
	0xE0, 0x05, 0x11, 0x14,
	0x37, 0x3F, 0x3F, 0x20,
	0x4F, 0x08, 0x0E, 0x0D,
	0x12, 0x14, 0x12, 0x14,
	0x1D, 0x1C, 0x05, 0x11,
	0x14, 0x37, 0x3F, 0x3F,
	0x20, 0x4F, 0x08, 0x0E,
	0x0D, 0x12, 0x14, 0x12,
	0x14, 0x1D, 0x1C, 0xFF,
};

static char hx8389bg_qhd_video_on_cmd8[] = {
	0x80, 0x00, 0x39, 0xC0,
	0xC1, 0x01, 0x00, 0x07,
	0x1C, 0x2A, 0x34, 0x3F,
	0x49, 0x52, 0x5B, 0x63,
	0x6A, 0x71, 0x78, 0x7F,
	0x85, 0x8B, 0x91, 0x97,
	0x9D, 0xA3, 0xA8, 0xAF,
	0xB5, 0xBE, 0xC6, 0xC9,
	0xCD, 0xD7, 0xE1, 0xE6,
	0xF1, 0xF7, 0xFF, 0x34,
	0xCB, 0x4F, 0xD9, 0x54,
	0xC8, 0x3C, 0x4A, 0xC0,
	0x00, 0x07, 0x1C, 0x2A,
	0x34, 0x3F, 0x49, 0x52,
	0x5B, 0x63, 0x6A, 0x71,
	0x78, 0x7F, 0x85, 0x8B,
	0x91, 0x97, 0x9D, 0xA3,
	0xA8, 0xAF, 0xB5, 0xBE,
	0xC6, 0xC9, 0xCD, 0xD7,
	0xE1, 0xE6, 0xF1, 0xF7,
	0xFF, 0x34, 0xCB, 0x4F,
	0xD9, 0x54, 0xC8, 0x3C,
	0x4A, 0xC0, 0x00, 0x07,
	0x1C, 0x2A, 0x34, 0x3F,
	0x49, 0x52, 0x5B, 0x63,
	0x6A, 0x71, 0x78, 0x7F,
	0x85, 0x8B, 0x91, 0x97,
	0x9D, 0xA3, 0xA8, 0xAF,
	0xB5, 0xBE, 0xC6, 0xC9,
	0xCD, 0xD7, 0xE1, 0xE6,
	0xF1, 0xF7, 0xFF, 0x34,
	0xCB, 0x4F, 0xD9, 0x54,
	0xC8, 0x3C, 0x4A, 0xC0,

};

static char hx8389bg_qhd_video_on_cmd9[] = {
	0x02, 0x00, 0x39, 0xC0,
	0xCC, 0x02, 0xFF, 0xFF,
};

static char hx8389bg_qhd_video_on_cmd10[] = {
	0x05, 0x00, 0x39, 0xC0,
	0xB6, 0x00, 0xa2, 0x00,
	0xa2, 0xFF, 0xFF, 0xFF,
};

static char hx8389bg_qhd_video_on_cmd11[] = {
	0x02, 0x00, 0x39, 0xC0,
	0xC6, 0x08, 0xFF, 0xFF,
};

static char hx8389bg_qhd_video_on_cmd12[] = {
	0x02, 0x00, 0x39, 0xC0,
	0x11, 0x00, 0xFF, 0xFF,
};

static char hx8389bg_qhd_video_on_cmd13[] = {
	0x02, 0x00, 0x39, 0xC0,
	0x29, 0x00, 0xFF, 0xFF,
};

static struct mipi_dsi_cmd hx8389bg_qhd_video_on_command[] = {
	{0x8, hx8389bg_qhd_video_on_cmd0, 0x00},
	{0xc, hx8389bg_qhd_video_on_cmd1, 0x00},
	{0x18, hx8389bg_qhd_video_on_cmd2, 0x00},
	{0xc, hx8389bg_qhd_video_on_cmd3, 0x00},
	{0x24, hx8389bg_qhd_video_on_cmd4, 0x00},
	{0x8, hx8389bg_qhd_video_on_cmd5, 0x00},
	{0x38, hx8389bg_qhd_video_on_cmd6, 0x00},
	{0x28, hx8389bg_qhd_video_on_cmd7, 0x00},
	{0x84, hx8389bg_qhd_video_on_cmd8, 0x00},
	{0x8, hx8389bg_qhd_video_on_cmd9, 0x00},
	{0xc, hx8389bg_qhd_video_on_cmd10, 0x00},
	{0x8, hx8389bg_qhd_video_on_cmd11, 0x00},
	{0x8, hx8389bg_qhd_video_on_cmd12, 0x78},
	{0x8, hx8389bg_qhd_video_on_cmd13, 0x32}
};

#define HX8389BG_QHD_VIDEO_ON_COMMAND 14


static char hx8389bg_qhd_videooff_cmd0[] = {
	0x28, 0x00, 0x05, 0x80
};

static char hx8389bg_qhd_videooff_cmd1[] = {
	0x10, 0x00, 0x05, 0x80
};

static struct mipi_dsi_cmd hx8389bg_qhd_video_off_command[] = {
	{0x4, hx8389bg_qhd_videooff_cmd0, 0x32},
	{0x4, hx8389bg_qhd_videooff_cmd1, 0x78}
};

#define HX8389BG_QHD_VIDEO_OFF_COMMAND 2


static struct command_state hx8389bg_qhd_video_state = {
	0, 1
};

/*---------------------------------------------------------------------------*/
/* Command mode panel information                                            */
/*---------------------------------------------------------------------------*/
static struct commandpanel_info hx8389bg_qhd_video_command_panel = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Video mode panel information                                              */
/*---------------------------------------------------------------------------*/
static struct videopanel_info hx8389bg_qhd_video_video_panel = {
	1, 0, 0, 0, 1, 1, 2, 0, 0x9
};

/*---------------------------------------------------------------------------*/
/* Lane configuration                                                        */
/*---------------------------------------------------------------------------*/
static struct lane_configuration hx8389bg_qhd_video_lane_config = {
	2, 0, 1, 1, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel timing                                                              */
/*---------------------------------------------------------------------------*/
static const uint32_t hx8389bg_qhd_video_timings[] = {
	0x73, 0x21, 0x1A, 0x00, 0x31, 0x2D, 0x1E, 0x23, 0x2B, 0x03, 0x04, 0x00
};

static struct panel_timing hx8389bg_qhd_video_timing_info = {
	0, 4, 0x1f, 0x2d
};

/*---------------------------------------------------------------------------*/
/* Panel reset sequence                                                      */
/*---------------------------------------------------------------------------*/
static struct panel_reset_sequence hx8389bg_qhd_video_reset_seq = {
	{1, 0, 1, }, {20, 2, 20, }, 2
};

/*---------------------------------------------------------------------------*/
/* Backlight setting                                                         */
/*---------------------------------------------------------------------------*/
static struct backlight hx8389bg_qhd_video_backlight = {
	1, 1, 4095, 100, 1, "PMIC_8909"
};

//#define hx8389bg_qhd_VIDEO_SIGNATURE 0xFFFF

#endif /*_PANEL_HX8389BG_QHD_VIDEO_H_*/

