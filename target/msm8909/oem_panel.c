/* Copyright (c) 2014-2015, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of The Linux Foundation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <debug.h>
#include <err.h>
#include <smem.h>
#include <msm_panel.h>
#include <board.h>
#include <mipi_dsi.h>
#include <target/display.h>
#include <platform/iomap.h>
#include <platform/gpio.h>
#include "include/panel.h"
#include "panel_display.h"

#include "include/panel_hx8394d_720p_video.h"
#include "include/panel_sharp_qhd_video.h"
#include "include/panel_truly_wvga_cmd.h"
#include "include/panel_hx8379a_fwvga_skua_video.h"
#include "include/panel_ili9806e_fwvga_video.h"
#include "include/panel_hx8394d_qhd_video.h"
#include "include/panel_hx8379c_fwvga_video.h"

#include "include/panel_nt35512_fwvga_video.h"
#include "include/panel_otm9605a_qhd_video.h"
#include "include/panel_otm9605a_qhd_cb03_video.h"
#include "include/panel_otm9605ag_qhd_video.h"
#include "include/panel_hx8389b_qhd_video.h"
#include "include/panel_hx8389bg_qhd_video.h"
#include "include/panel_jd9161ba_fwvga_video.h"

#include "include/panel_hzguct_ili9881_720p_video.h"
#include "include/panel_hzguct_rm68191_qhd_video.h"
#include "include/panel_idata_otm1287a_720p_video.h"
#include "include/panel_innolux_zj070na_1024x600_video.h"

#define LCD_CHIP_ID_SWITCH 0

#if LCD_CHIP_ID_SWITCH

#define DISPLAY_MAX_PANEL_DETECTION 5

#else

#define DISPLAY_MAX_PANEL_DETECTION 0

#endif


#define ILI9806E_FWVGA_VIDEO_PANEL_POST_INIT_DELAY 68

enum {
	QRD_SKUA = 0x00,
	QRD_SKUC = 0x08,
	QRD_SKUE = 0x09,
	QRD_SKUE_CB03 = 0x0C,
	QRD_SKUE_CB03_JP = 0x0D,
};

#if LCD_CHIP_ID_SWITCH
/*---------------------------------------------------------------------------*/
/* static panel selection variable                                           */
/*---------------------------------------------------------------------------*/
static uint32_t auto_pan_loop = 0;
#endif

enum {
	INNOLUX_ZJ070NA_1024x600_PANEL,
	HX8394D_720P_VIDEO_PANEL,
	SHARP_QHD_VIDEO_PANEL,
	TRULY_WVGA_CMD_PANEL,
	HX8379A_FWVGA_SKUA_VIDEO_PANEL,
	ILI9806E_FWVGA_VIDEO_PANEL,
	HX8394D_QHD_VIDEO_PANEL,
	HX8379C_FWVGA_VIDEO_PANEL,
	NT35512_FWVGA_VIDEO_PANEL,
	OTM9605A_QHD_VIDEO_PANEL,
	OTM9605A_QHD_CB03_VIDEO_PANEL,
	OTM9605AG_QHD_VIDEO_PANEL,
	HX8389B_QHD_VIDEO_PANEL,
	HX8389BG_QHD_VIDEO_PANEL,
	JD9161BA_FWVGA_VIDEO_PANEL,
	HZGUCT_ILI9881_720P_VIDEO_PANEL,
	HZGUCT_RM68191_QHD_VIDEO_PANEL,
	IDATA_OTM1287A_720P_VIDEO_PANEL,
	UNKNOWN_PANEL
};

/*
 * The list of panels that are supported on this target.
 * Any panel in this list can be selected using fastboot oem command.
 */
static struct panel_list supp_panels[] = {
	{"innolux_zj070na_1024x600",INNOLUX_ZJ070NA_1024x600_PANEL},
	{"hx8394d_720p_video", HX8394D_720P_VIDEO_PANEL},
	{"sharp_qhd_video", SHARP_QHD_VIDEO_PANEL},
	{"truly_wvga_cmd", TRULY_WVGA_CMD_PANEL},
	{"hx8379a_fwvga_skua_video", HX8379A_FWVGA_SKUA_VIDEO_PANEL},
	{"ili9806e_fwvga_video",ILI9806E_FWVGA_VIDEO_PANEL},
	{"hx8394d_qhd_video", HX8394D_QHD_VIDEO_PANEL},
	{"hx8379c_fwvga_video",HX8379C_FWVGA_VIDEO_PANEL},
	{"nt35512_fwvga_video",NT35512_FWVGA_VIDEO_PANEL},
	{"otm9605a_qhd_video_panel",OTM9605A_QHD_VIDEO_PANEL},
	{"otm9605a_qhd_cb03_video_panel",OTM9605A_QHD_CB03_VIDEO_PANEL},
	{"otm9605ag_qhd_video_panel",OTM9605AG_QHD_VIDEO_PANEL},
	{"hx8389b_qhd_video_panel",HX8389B_QHD_VIDEO_PANEL},
	{"hx8389bg_qhd_video_panel",HX8389BG_QHD_VIDEO_PANEL},
	{"jd9161ba_fwvga_video_panel",JD9161BA_FWVGA_VIDEO_PANEL},
	{"hzguct_ili9881_720p_video_panel",HZGUCT_ILI9881_720P_VIDEO_PANEL},
	{"hzguct_rm68191_qhd_video_panel",HZGUCT_RM68191_QHD_VIDEO_PANEL},
	{"idata_otm1287a_720p_video_panel",IDATA_OTM1287A_720P_VIDEO_PANEL},
};

static uint32_t panel_id;

int oem_panel_rotation()
{
	return NO_ERROR;
}

int oem_panel_on()
{
	/*
	 * OEM can keep there panel specific on instructions in this
	 * function
	 */
	if (panel_id == ILI9806E_FWVGA_VIDEO_PANEL)
		mdelay(ILI9806E_FWVGA_VIDEO_PANEL_POST_INIT_DELAY);

	return NO_ERROR;
}

int oem_panel_off()
{
	/*
	 * OEM can keep their panel specific off instructions
	 * in this function
	 */
	return NO_ERROR;
}

static int init_panel_data(struct panel_struct *panelstruct,
			struct msm_panel_info *pinfo,
			struct mdss_dsi_phy_ctrl *phy_db)
{
	int pan_type = PANEL_TYPE_DSI;

	dprintf(INFO, "init_panel_data: panel={%d, %s}\n", panel_id, innolux_zj070na_1024x600_video_panel_data.panel_node_id);
	switch (panel_id) {
	case INNOLUX_ZJ070NA_1024x600_PANEL:
		panelstruct->paneldata = &innolux_zj070na_1024x600_video_panel_data;
		panelstruct->panelres = &innolux_zj070na_1024x600_video_panel_res;
		panelstruct->color = &innolux_zj070na_1024x600_video_color;
		panelstruct->videopanel = &innolux_zj070na_1024x600_video_video_panel;
		panelstruct->commandpanel = &innolux_zj070na_1024x600_video_command_panel;
		panelstruct->state = &innolux_zj070na_1024x600_video_state;
		panelstruct->laneconfig = &innolux_zj070na_1024x600_video_lane_config;
		panelstruct->paneltiminginfo = &innolux_zj070na_1024x600_video_timing_info;
		panelstruct->panelresetseq = &innolux_zj070na_1024x600_video_reset_seq;
		panelstruct->backlightinfo = &innolux_zj070na_1024x600_video_backlight;
		pinfo->mipi.panel_cmds = 0;
		pinfo->mipi.num_of_panel_cmds = 0;
		memcpy(phy_db->timing, innolux_zj070na_1024x600_video_timings, TIMING_SIZE);
		panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case HX8394D_720P_VIDEO_PANEL:
		panelstruct->paneldata	  = &hx8394d_720p_video_panel_data;
		panelstruct->panelres	  = &hx8394d_720p_video_panel_res;
		panelstruct->color		  = &hx8394d_720p_video_color;
		panelstruct->videopanel   = &hx8394d_720p_video_video_panel;
		panelstruct->commandpanel = &hx8394d_720p_video_command_panel;
		panelstruct->state		  = &hx8394d_720p_video_state;
		panelstruct->laneconfig   = &hx8394d_720p_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8394d_720p_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8394d_720p_video_panel_reset_seq;
		panelstruct->backlightinfo = &hx8394d_720p_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8394d_720p_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8394D_720P_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				hx8394d_720p_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8394D_720P_VIDEO_SIGNATURE;
		break;
        case SHARP_QHD_VIDEO_PANEL:
		panelstruct->paneldata    = &sharp_qhd_video_panel_data;
		panelstruct->panelres     = &sharp_qhd_video_panel_res;
		panelstruct->color        = &sharp_qhd_video_color;
		panelstruct->videopanel   = &sharp_qhd_video_video_panel;
		panelstruct->commandpanel = &sharp_qhd_video_command_panel;
		panelstruct->state        = &sharp_qhd_video_state;
		panelstruct->laneconfig   = &sharp_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					= &sharp_qhd_video_timing_info;
		panelstruct->panelresetseq
					= &sharp_qhd_video_panel_reset_seq;
		panelstruct->backlightinfo = &sharp_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					= sharp_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= SHARP_QHD_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing, sharp_qhd_video_timings, TIMING_SIZE);
		break;
      case TRULY_WVGA_CMD_PANEL:
               panelstruct->paneldata    = &truly_wvga_cmd_panel_data;
               panelstruct->panelres     = &truly_wvga_cmd_panel_res;
               panelstruct->color        = &truly_wvga_cmd_color;
               panelstruct->videopanel   = &truly_wvga_cmd_video_panel;
               panelstruct->commandpanel = &truly_wvga_cmd_command_panel;
               panelstruct->state        = &truly_wvga_cmd_state;
               panelstruct->laneconfig   = &truly_wvga_cmd_lane_config;
               panelstruct->paneltiminginfo
                                        = &truly_wvga_cmd_timing_info;
               panelstruct->panelresetseq
                                        = &truly_wvga_cmd_reset_seq;
               panelstruct->backlightinfo = &truly_wvga_cmd_backlight;
               pinfo->mipi.panel_cmds
                                       = truly_wvga_cmd_on_command;
               pinfo->mipi.num_of_panel_cmds
                                      = TRULY_WVGA_CMD_ON_COMMAND;
               memcpy(phy_db->timing,
                       truly_wvga_cmd_timings, TIMING_SIZE);
               break;
	case HX8379A_FWVGA_SKUA_VIDEO_PANEL:
		panelstruct->paneldata	  = &hx8379a_fwvga_skua_video_panel_data;
		panelstruct->panelres	  = &hx8379a_fwvga_skua_video_panel_res;
		panelstruct->color	  = &hx8379a_fwvga_skua_video_color;
		panelstruct->videopanel   = &hx8379a_fwvga_skua_video_video_panel;
		panelstruct->commandpanel = &hx8379a_fwvga_skua_video_command_panel;
		panelstruct->state	  = &hx8379a_fwvga_skua_video_state;
		panelstruct->laneconfig   = &hx8379a_fwvga_skua_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8379a_fwvga_skua_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8379a_fwvga_skua_video_reset_seq;
		panelstruct->backlightinfo = &hx8379a_fwvga_skua_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8379a_fwvga_skua_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8379A_FWVGA_SKUA_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				hx8379a_fwvga_skua_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8379A_FWVGA_SKUA_VIDEO_SIGNATURE;
		break;
	case ILI9806E_FWVGA_VIDEO_PANEL:
        panelstruct->paneldata    = &ili9806e_fwvga_video_panel_data;
        panelstruct->panelres     = &ili9806e_fwvga_video_panel_res;
        panelstruct->color        = &ili9806e_fwvga_video_color;
        panelstruct->videopanel   = &ili9806e_fwvga_video_video_panel;
        panelstruct->commandpanel = &ili9806e_fwvga_video_command_panel;
        panelstruct->state        = &ili9806e_fwvga_video_state;
        panelstruct->laneconfig   = &ili9806e_fwvga_video_lane_config;
        panelstruct->paneltiminginfo
                                 = &ili9806e_fwvga_video_timing_info;
        panelstruct->panelresetseq
                                 = &ili9806e_fwvga_video_reset_seq;
        panelstruct->backlightinfo = &ili9806e_fwvga_video_backlight;
        pinfo->mipi.panel_cmds
                                = ili9806e_fwvga_video_on_command;
        pinfo->mipi.num_of_panel_cmds
                                = ILI9806E_FWVGA_VIDEO_ON_COMMAND;
        memcpy(phy_db->timing,
                        ili9806e_fwvga_video_timings, TIMING_SIZE);
        pinfo->mipi.signature = ILI9806E_FWVGA_VIDEO_SIGNATURE;
        break;
	case HZGUCT_ILI9881_720P_VIDEO_PANEL:
         panelstruct->paneldata    = &hzguct_ili9881_720p_video_panel_data;
         panelstruct->panelres     = &hzguct_ili9881_720p_video_panel_res;
         panelstruct->color        = &hzguct_ili9881_720p_video_color;
         panelstruct->videopanel   = &hzguct_ili9881_720p_video_video_panel;
         panelstruct->commandpanel = &hzguct_ili9881_720p_video_command_panel;
         panelstruct->state        = &hzguct_ili9881_720p_video_state;
         panelstruct->laneconfig   = &hzguct_ili9881_720p_video_lane_config;
         panelstruct->paneltiminginfo
                                  = &hzguct_ili9881_720p_video_timing_info;
         panelstruct->panelresetseq
                                  = &hzguct_ili9881_720p_video_reset_seq;
         panelstruct->backlightinfo = &hzguct_ili9881_720p_video_backlight;
         pinfo->mipi.panel_cmds
                                 = hzguct_ili9881_720p_video_on_command;
         pinfo->mipi.num_of_panel_cmds
                                 = HZGUCT_ILI9881_720P_VIDEO_ON_COMMAND;
         memcpy(phy_db->timing,
                         hzguct_ili9881_720p_video_timings, TIMING_SIZE);
         pinfo->mipi.signature = HZGUCT_ILI9881_720P_VIDEO_SIGNATURE;
         break;
	case HZGUCT_RM68191_QHD_VIDEO_PANEL:
         panelstruct->paneldata    = &hzguct_rm68191_qhd_video_panel_data;
         panelstruct->panelres     = &hzguct_rm68191_qhd_video_panel_res;
         panelstruct->color        = &hzguct_rm68191_qhd_video_color;
         panelstruct->videopanel   = &hzguct_rm68191_qhd_video_video_panel;
         panelstruct->commandpanel = &hzguct_rm68191_qhd_video_command_panel;
         panelstruct->state        = &hzguct_rm68191_qhd_video_state;
         panelstruct->laneconfig   = &hzguct_rm68191_qhd_video_lane_config;
         panelstruct->paneltiminginfo
                                  = &hzguct_rm68191_qhd_video_timing_info;
         panelstruct->panelresetseq
                                  = &hzguct_rm68191_qhd_video_reset_seq;
         panelstruct->backlightinfo = &hzguct_rm68191_qhd_video_backlight;
         pinfo->mipi.panel_cmds
                                 = hzguct_rm68191_qhd_video_on_command;
         pinfo->mipi.num_of_panel_cmds
                                 = HZGUCT_RM68191_QHD_VIDEO_ON_COMMAND;
         memcpy(phy_db->timing,
                         hzguct_rm68191_qhd_video_timings, TIMING_SIZE);
         pinfo->mipi.signature = HZGUCT_RM68191_QHD_VIDEO_SIGNATURE;
         break;
	case HX8394D_QHD_VIDEO_PANEL:
		panelstruct->paneldata	  = &hx8394d_qhd_video_panel_data;
		panelstruct->panelres	  = &hx8394d_qhd_video_panel_res;
		panelstruct->color		  = &hx8394d_qhd_video_color;
		panelstruct->videopanel   = &hx8394d_qhd_video_video_panel;
		panelstruct->commandpanel = &hx8394d_qhd_video_command_panel;
		panelstruct->state		  = &hx8394d_qhd_video_state;
		panelstruct->laneconfig   = &hx8394d_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8394d_qhd_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8394d_qhd_video_panel_reset_seq;
		panelstruct->backlightinfo = &hx8394d_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8394d_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8394D_QHD_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
				hx8394d_qhd_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = HX8394D_QHD_VIDEO_SIGNATURE;
		break;
	case HX8379C_FWVGA_VIDEO_PANEL:
		panelstruct->paneldata    = &hx8379c_fwvga_video_panel_data;
		panelstruct->panelres     = &hx8379c_fwvga_video_panel_res;
		panelstruct->color        = &hx8379c_fwvga_video_color;
		panelstruct->videopanel   = &hx8379c_fwvga_video_video_panel;
		panelstruct->commandpanel = &hx8379c_fwvga_video_command_panel;
		panelstruct->state        = &hx8379c_fwvga_video_state;
		panelstruct->laneconfig   = &hx8379c_fwvga_video_lane_config;
		panelstruct->paneltiminginfo
					= &hx8379c_fwvga_video_timing_info;
		panelstruct->panelresetseq
					= &hx8379c_fwvga_video_reset_seq;
		panelstruct->backlightinfo = &hx8379c_fwvga_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8379c_fwvga_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8379C_FWVGA_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
					hx8379c_fwvga_video_timings, TIMING_SIZE);
		//pinfo->mipi.signature = HX8379C_FWVGA_VIDEO_SIGNATURE;
		break;
	case NT35512_FWVGA_VIDEO_PANEL:
		panelstruct->paneldata    = &nt35512_fwvga_video_panel_data;
		panelstruct->panelres     = &nt35512_fwvga_video_panel_res;
		panelstruct->color        = &nt35512_fwvga_video_color;
		panelstruct->videopanel   = &nt35512_fwvga_video_video_panel;
		panelstruct->commandpanel = &nt35512_fwvga_video_command_panel;
		panelstruct->state        = &nt35512_fwvga_video_state;
		panelstruct->laneconfig   = &nt35512_fwvga_video_lane_config;
		panelstruct->paneltiminginfo
					= &nt35512_fwvga_video_timing_info;
		panelstruct->panelresetseq
					= &nt35512_fwvga_video_reset_seq;
		panelstruct->backlightinfo = &nt35512_fwvga_video_backlight;
		pinfo->mipi.panel_cmds
					= nt35512_fwvga_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= NT35512_FWVGA_VIDEO_ON_COMMAND;
		memcpy(phy_db->timing,
					nt35512_fwvga_video_timings, TIMING_SIZE);
		break;
	case OTM9605A_QHD_VIDEO_PANEL:
		panelstruct->paneldata    = & otm9605a_qhd_video_panel_data;
		panelstruct->panelres     = & otm9605a_qhd_video_panel_res;
		panelstruct->color        = & otm9605a_qhd_video_color;
		panelstruct->videopanel   = & otm9605a_qhd_video_video_panel;
		panelstruct->commandpanel = & otm9605a_qhd_video_command_panel;
		panelstruct->state        = & otm9605a_qhd_video_state;
		panelstruct->laneconfig   = & otm9605a_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					 = & otm9605a_qhd_video_timing_info;
		panelstruct->panelresetseq
					 = & otm9605a_qhd_video_reset_seq;
		panelstruct->backlightinfo = & otm9605a_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					=  otm9605a_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= OTM9605A_QHD_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 otm9605a_qhd_video_timings, TIMING_SIZE);

	    	panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case OTM9605A_QHD_CB03_VIDEO_PANEL:
		panelstruct->paneldata    = & otm9605a_qhd_cb03_video_panel_data;
		panelstruct->panelres     = & otm9605a_qhd_cb03_video_panel_res;
		panelstruct->color        = & otm9605a_qhd_cb03_video_color;
		panelstruct->videopanel   = & otm9605a_qhd_cb03_video_video_panel;
		panelstruct->commandpanel = & otm9605a_qhd_cb03_video_command_panel;
		panelstruct->state        = & otm9605a_qhd_cb03_video_state;
		panelstruct->laneconfig   = & otm9605a_qhd_cb03_video_lane_config;
		panelstruct->paneltiminginfo
					 = & otm9605a_qhd_cb03_video_timing_info;
		panelstruct->panelresetseq
					 = & otm9605a_qhd_cb03_video_reset_seq;
		panelstruct->backlightinfo = & otm9605a_qhd_cb03_video_backlight;
		pinfo->mipi.panel_cmds
					=  otm9605a_qhd_cb03_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= OTM9605A_QHD_CB03_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 otm9605a_qhd_cb03_video_timings, TIMING_SIZE);
		pinfo->mipi.signature = OTM9605A_QHD_CB03_VIDEO_SIGNATURE;
	    panelstruct->paneldata->panel_lp11_init = 1;
		break;	
	case OTM9605AG_QHD_VIDEO_PANEL:
		panelstruct->paneldata    = &otm9605ag_qhd_video_panel_data;
		panelstruct->panelres     = &otm9605ag_qhd_video_panel_res;
		panelstruct->color        = &otm9605ag_qhd_video_color;
		panelstruct->videopanel   = &otm9605ag_qhd_video_video_panel;
		panelstruct->commandpanel = &otm9605ag_qhd_video_command_panel;
		panelstruct->state        = &otm9605ag_qhd_video_state;
		panelstruct->laneconfig   = &otm9605ag_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					 = & otm9605ag_qhd_video_timing_info;
		panelstruct->panelresetseq
					 = & otm9605ag_qhd_video_reset_seq;
		panelstruct->backlightinfo = & otm9605ag_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					=  otm9605ag_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= OTM9605AG_QHD_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 otm9605ag_qhd_video_timings, TIMING_SIZE);

	    panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case IDATA_OTM1287A_720P_VIDEO_PANEL:
		panelstruct->paneldata    = &idata_otm1287a_720p_video_panel_data;
		panelstruct->panelres     = &idata_otm1287a_720p_video_panel_res;
		panelstruct->color        = &idata_otm1287a_720p_video_color;
		panelstruct->videopanel   = &idata_otm1287a_720p_video_video_panel;
		panelstruct->commandpanel = &idata_otm1287a_720p_video_command_panel;
		panelstruct->state        = &idata_otm1287a_720p_video_state;
		panelstruct->laneconfig   = &idata_otm1287a_720p_video_lane_config;
		panelstruct->paneltiminginfo
					 = & idata_otm1287a_720p_video_timing_info;
		panelstruct->panelresetseq
					 = & idata_otm1287a_720p_video_reset_seq;
		panelstruct->backlightinfo = & idata_otm1287a_720p_video_backlight;
		pinfo->mipi.panel_cmds
					=  idata_otm1287a_720p_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= IDATA_OTM1287A_720P_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 idata_otm1287a_720p_video_timings, TIMING_SIZE);

		//panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case HX8389B_QHD_VIDEO_PANEL:
		panelstruct->paneldata    = &hx8389b_qhd_video_panel_data;
		panelstruct->panelres     = &hx8389b_qhd_video_panel_res;
		panelstruct->color        = &hx8389b_qhd_video_color;
		panelstruct->videopanel   = &hx8389b_qhd_video_video_panel;
		panelstruct->commandpanel = &hx8389b_qhd_video_command_panel;
		panelstruct->state        = &hx8389b_qhd_video_state;
		panelstruct->laneconfig   = &hx8389b_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8389b_qhd_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8389b_qhd_video_reset_seq;
		panelstruct->backlightinfo = &hx8389b_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8389b_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8389B_QHD_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 hx8389b_qhd_video_timings, TIMING_SIZE);

	    //panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case HX8389BG_QHD_VIDEO_PANEL:
		panelstruct->paneldata    = &hx8389bg_qhd_video_panel_data;
		panelstruct->panelres     = &hx8389bg_qhd_video_panel_res;
		panelstruct->color        = &hx8389bg_qhd_video_color;
		panelstruct->videopanel   = &hx8389bg_qhd_video_video_panel;
		panelstruct->commandpanel = &hx8389bg_qhd_video_command_panel;
		panelstruct->state        = &hx8389bg_qhd_video_state;
		panelstruct->laneconfig   = &hx8389bg_qhd_video_lane_config;
		panelstruct->paneltiminginfo
					 = &hx8389bg_qhd_video_timing_info;
		panelstruct->panelresetseq
					 = &hx8389bg_qhd_video_reset_seq;
		panelstruct->backlightinfo = &hx8389bg_qhd_video_backlight;
		pinfo->mipi.panel_cmds
					= hx8389bg_qhd_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= HX8389BG_QHD_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 hx8389bg_qhd_video_timings, TIMING_SIZE);

	    //panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case JD9161BA_FWVGA_VIDEO_PANEL:
		panelstruct->paneldata    = &jd9161ba_fwvga_video_panel_data;
		panelstruct->panelres     = &jd9161ba_fwvga_video_panel_res;
		panelstruct->color        = &jd9161ba_fwvga_video_color;
		panelstruct->videopanel   = &jd9161ba_fwvga_video_video_panel;
		panelstruct->commandpanel = &jd9161ba_fwvga_video_command_panel;
		panelstruct->state        = &jd9161ba_fwvga_video_state;
		panelstruct->laneconfig   = &jd9161ba_fwvga_video_lane_config;
		panelstruct->paneltiminginfo
					 = &jd9161ba_fwvga_video_timing_info;
		panelstruct->panelresetseq
					 = &jd9161ba_fwvga_video_reset_seq;
		panelstruct->backlightinfo = &jd9161ba_fwvga_video_backlight;
		pinfo->mipi.panel_cmds
					= jd9161ba_fwvga_video_on_command;
		pinfo->mipi.num_of_panel_cmds
					= JD9161BA_FWVGA_VIDEO_ON_COMMAND ;
		memcpy(phy_db->timing,
				 jd9161ba_fwvga_video_timings, TIMING_SIZE);

	    //panelstruct->paneldata->panel_lp11_init = 1;
		break;
	case UNKNOWN_PANEL:
	default:
		memset(panelstruct, 0, sizeof(struct panel_struct));
		memset(pinfo->mipi.panel_cmds, 0, sizeof(struct mipi_dsi_cmd));
		pinfo->mipi.num_of_panel_cmds = 0;
		memset(phy_db->timing, 0, TIMING_SIZE);
		pan_type = PANEL_TYPE_UNKNOWN;
		break;
	}
	return pan_type;
}


uint32_t oem_panel_max_auto_detect_panels()
{
        return target_panel_auto_detect_enabled() ? DISPLAY_MAX_PANEL_DETECTION : 0;
}


int oem_panel_select(const char *panel_name, struct panel_struct *panelstruct,
			struct msm_panel_info *pinfo,
			struct mdss_dsi_phy_ctrl *phy_db)
{
	uint32_t hw_id = board_hardware_id();
	uint32_t platform_subtype = board_hardware_subtype();
	int32_t panel_override_id;

	if (panel_name) {
		panel_override_id = panel_name_to_id(supp_panels,
				ARRAY_SIZE(supp_panels), panel_name);

		if (panel_override_id < 0) {
			dprintf(CRITICAL, "Not able to search the panel:%s\n",
					 panel_name + strspn(panel_name, " "));
		} else if (panel_override_id < UNKNOWN_PANEL) {
			/* panel override using fastboot oem command */
			panel_id = panel_override_id;

			dprintf(INFO, "OEM panel override:%s\n",
					panel_name + strspn(panel_name, " "));
			goto panel_init;
		}
	}
	dprintf(INFO, "oem_panel_select: hw_id=%d, subtype=%d\n", hw_id, platform_subtype);
	switch (hw_id) {
	case HW_PLATFORM_SURF:
	case HW_PLATFORM_MTP:
	case HW_PLATFORM_RCM:
		panel_id = HX8394D_720P_VIDEO_PANEL;
		break;
	case HW_PLATFORM_QRD:		
		switch (platform_subtype){
			case QRD_SKUA:
				panel_id = HX8379A_FWVGA_SKUA_VIDEO_PANEL;
				break;
			case QRD_SKUC:
				panel_id = ILI9806E_FWVGA_VIDEO_PANEL;
				break;
			case QRD_SKUE:
				panel_id = OTM9605AG_QHD_VIDEO_PANEL;
				break;
			case QRD_SKUE_CB03:
				//panel_id = OTM9605A_QHD_CB03_VIDEO_PANEL;
				panel_id = INNOLUX_ZJ070NA_1024x600_PANEL;
				#if LCD_CHIP_ID_SWITCH
				switch (auto_pan_loop) {
				case 0:
					panel_id = OTM9605A_QHD_CB03_VIDEO_PANEL;
					break;

				case 1:
					panel_id = HZGUCT_ILI9881_720P_VIDEO_PANEL;
					break;

				case 2:
					panel_id = HZGUCT_RM68191_QHD_VIDEO_PANEL;
					break;

				case 3:
					panel_id = HX8389BG_QHD_VIDEO_PANEL;
					break;

				case 4:
					panel_id = NT35512_FWVGA_VIDEO_PANEL;
					break;

				default:
					panel_id = OTM9605A_QHD_CB03_VIDEO_PANEL;
					break;
				}
				auto_pan_loop++;
				#endif
				break;
			case QRD_SKUE_CB03_JP:
				panel_id = OTM9605A_QHD_CB03_VIDEO_PANEL;
				break;
			default:
				dprintf(CRITICAL, "QRD Display not enabled for %d type subtype\n",platform_subtype);
				return PANEL_TYPE_UNKNOWN;
		}
		break;
	default:
		dprintf(CRITICAL, "Display not enabled for %d HW type\n",hw_id);
		return PANEL_TYPE_UNKNOWN;
	}

panel_init:
	phy_db->regulator_mode = DSI_PHY_REGULATOR_LDO_MODE;
	return init_panel_data(panelstruct, pinfo, phy_db);
}
